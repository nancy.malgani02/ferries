﻿using System;
using System.Threading;
using System.Windows.Forms;
using CommandLine;
using CommandLine.Text;
using TestRunner;

namespace TestRunnerWin
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string[] args = Environment.GetCommandLineArgs();
            var options = new Options();
            if (args.Length > 1)
            {
                if (Parser.Default.ParseArguments(args, options))
                {
                    Runner runner = new Runner();
                    runner.Run(
                        options.Config,
                        options.Project,
                        options.DLL,
                        options.TagsInclude,
                        options.TagsExclude,
                        options.CreateSpecFlowReport,
                        options.CreateNUnitReport,
                        options.CreateReportUnit,
                        true,
                        true,
                        options.OverrideArtifactSubDir);
                }
                else
                {
                    Console.WriteLine(options.GetUsage());
                }
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new RunnerForm());
            }
        }

        public static void OpenNewRunnerForm()
        {
            Thread t = new Thread(new ThreadStart(RunnerForm));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        private static void RunnerForm()
        {
            Application.Run(new RunnerForm());
        }
    }

    public class Options
    {
        [Option('c', "config", Required = true, HelpText = "Test Config to pass to the testing framework.")]
        public string Config { get; set; }

        [Option('i', "includetags", Required = false, HelpText = "Comma separated list of tags to include.")]
        public string TagsInclude { get; set; }

        [Option('e', "includetags", Required = false, HelpText = "Comma separated list of tags to exclude.")]
        public string TagsExclude { get; set; }

        [Option('p', "project", Required = true, HelpText = "The csproject containing the specflow features. Usually the csproj file corresponding to the DLL file being tested.")]
        public string Project { get; set; }

        [Option('d', "dll", Required = true, HelpText = "The DLL file containing the tests.")]
        public string DLL { get; set; }

        [Option('n', "nunitreport", Required = false, DefaultValue = false, HelpText = "Create an nunit report.")]
        public bool CreateNUnitReport { get; set; }

        [Option('r', "reportunit", Required = false, DefaultValue = false, HelpText = "Create a report unit.")]
        public bool CreateReportUnit { get; set; }

        [Option('s', "specflowreport", Required = false, DefaultValue = false, HelpText = "Create a specflow report.")]
        public bool CreateSpecFlowReport { get; set; }

        [Option('a', "artifactoverride", Required = true, HelpText = "Override the default subdirectory where artifacts will be stored.")]
        public string OverrideArtifactSubDir { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}
