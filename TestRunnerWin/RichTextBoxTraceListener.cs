﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace TestRunnerWin
{
    class RichTextBoxTraceListener : TraceListener
    {
        private RichTextBox output;
        private TraceEventType traceType = TraceEventType.Information;
		private bool isMono;
        private bool isFirstTracePart = false;

        public RichTextBoxTraceListener(RichTextBox output, string name)
        {
            this.Name = name;
            this.output = output;

			if (Type.GetType ("Mono.Runtime") != null) 
			{
				isMono = true;
			} else
			{
				isMono = false;
			}
        }

        public override void Write(string message)
        {
            Action append = delegate()
            {
                this.FormatAndWrite(message);
            };

            if (output.InvokeRequired)
            {
                output.BeginInvoke(append);
            } else
            {
                append();
            }
        }

        private void FormatAndWrite(string message)
        {
            this.isFirstTracePart = false;
            
            if (message.StartsWith ("Runner"))
			{
                this.isFirstTracePart = true;
                
                if (message.Contains ("Runner Error"))
				{
					traceType = TraceEventType.Error;
				}
				else if (message.Contains ("Runner Start"))
				{
					traceType = TraceEventType.Start;
				}
				else if (message.Contains ("Runner Information"))
				{
					traceType = TraceEventType.Information;
				}
				else if (message.Contains ("Runner Transfer")) 
				{
					traceType = TraceEventType.Transfer;
				}
			}
	            
			if (this.isMono)
			{
				this.output.AppendText (message, this.GetTraceColour(traceType));
            }
			else
			{
                if (this.isFirstTracePart)
                    return;
				
                this.output.AppendText (message, this.GetTraceColour(traceType));
			}
        }

        private Color GetTraceColour(TraceEventType traceType)
        {
            switch (traceType)
            {
                case TraceEventType.Error:
                    return Color.Red;
                case TraceEventType.Start:
                    return Color.Green;
                case TraceEventType.Transfer:
                    return Color.Blue;
                default:
                    return Color.Black;
            }
        }

        public override void WriteLine(string message) {
            Write(message + Environment.NewLine);
        }
    }

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;
            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
