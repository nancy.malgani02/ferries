﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using TestRunner;
using TestRunnerWin.Properties;

namespace TestRunnerWin
{
    public partial class RunnerForm : Form
    {
        private RichTextBoxTraceListener listener;
        private bool isMono = false;

        public RunnerForm()
        {
            InitializeComponent();

            if (Type.GetType("Mono.Runtime") != null)
            {
                isMono = true;
            }
            
            LoadSettings();
            listener = new RichTextBoxTraceListener(this.rtbOutput, "TestRunnerWin");
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            string config = this.txtConfig.Text;
            string project = this.txtProject.Text;
            string dll = this.txtDLL.Text;
            string tagsInclude = this.txtTagsInclude.Text;
            string tagsExclude = this.txtTagsExclude.Text;
            bool open = this.chkAutoOpen.Checked;
            bool createNunitReport = this.chkNunitReport.Checked;
            bool createReportUnit = this.chkReportUnit.Checked;
            bool createSpecReport = this.chkSpecFlowReport.Checked;
            string artifactSubDir = this.chkArtifactOverride.Checked ? this.txtArtifactSubDir.Text : String.Empty;
            string appiumURI = this.chkAppiumURI.Checked ? this.txtAppiumURI.Text : string.Empty;

            Runner runner = new Runner();
            
            runner.AddTraceListener(listener);

            BeforeRun();
            runner.RunComplete += Runner_RunComplete;
            runner.Run(
                config,
                project,
                dll,
                tagsInclude,
                tagsExclude,
                createSpecReport,
                createNunitReport,
                createReportUnit,
                open,
                true,
                artifactSubDir,
                appiumURI);
        }

        private void BeforeRun()
        {
            this.btnRun.Enabled = 
                this.btnChooseConfig.Enabled = 
                this.btnChooseDLL.Enabled = 
                this.btnChooseProject.Enabled = 
                this.txtArtifactSubDir.Enabled = 
                this.txtConfig.Enabled = 
                this.txtDLL.Enabled = 
                this.txtProject.Enabled = 
                this.txtTagsExclude.Enabled = 
                this.chkArtifactOverride.Enabled = 
                this.chkAutoOpen.Enabled = 
                this.chkNunitReport.Enabled = 
                this.chkSpecFlowReport.Enabled = 
                this.btnSaveSettings.Enabled =
                this.chkAppiumURI.Enabled =
                this.txtAppiumURI.Enabled = 
                this.txtTagsInclude.Enabled = false;
        }

        private void AfterRun()
        {
            this.btnRun.Enabled =
                this.btnChooseConfig.Enabled =
                this.btnChooseDLL.Enabled =
                this.btnChooseProject.Enabled =
                this.txtArtifactSubDir.Enabled =
                this.txtConfig.Enabled =
                this.txtDLL.Enabled =
                this.txtProject.Enabled =
                this.txtTagsExclude.Enabled =
                this.chkArtifactOverride.Enabled =
                this.chkAutoOpen.Enabled =
                this.chkNunitReport.Enabled =
                this.chkSpecFlowReport.Enabled =
                this.btnSaveSettings.Enabled =
                this.chkAppiumURI.Enabled =
                this.txtAppiumURI.Enabled =
                this.txtTagsInclude.Enabled = true;
        }

        void Runner_RunComplete(object sender, EventArgs e)
        {
            ((Runner) sender).RemoveTraceListener(listener);
            AfterRun();
        }

        private void btnChooseConfig_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (!String.IsNullOrWhiteSpace(this.txtConfig.Text))
            {
                ofd.FileName = this.txtConfig.Text;
                ofd.InitialDirectory = Path.GetDirectoryName(this.txtConfig.Text);
            }
            
            ofd.Filter = "json files (*.json)|*.json";
            
            if (ofd.ShowDialog() ==  DialogResult.OK)
            {
                this.txtConfig.Text = ofd.FileName;
            }
        }

        private void btnChooseProject_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (!String.IsNullOrWhiteSpace(this.txtProject.Text))
            {
                ofd.FileName = this.txtProject.Text;
                ofd.InitialDirectory = Path.GetDirectoryName(this.txtProject.Text);
            }

            ofd.Filter = "csproj files (*.csproj)|*.csproj";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.txtProject.Text = ofd.FileName;
            }
        }

        private void btnChooseDLL_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (!String.IsNullOrWhiteSpace(this.txtDLL.Text))
            {
                ofd.FileName = this.txtDLL.Text;
                ofd.InitialDirectory = Path.GetDirectoryName(this.txtDLL.Text);
            }

            ofd.Filter = "dll files (*.dll)|*.dll";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.txtDLL.Text = ofd.FileName;
            }
        }

        private void LoadSettings()
        {
            if (isMono)
            {
                this.LoadMonoSettings();
            }
            else
            {
                this.txtDLL.Text = Settings.Default.lastDLL;
                this.txtConfig.Text = Settings.Default.lastConfig;
                this.txtProject.Text = Settings.Default.lastProject;
                this.txtTagsInclude.Text = Settings.Default.lastSpecflowInclude;
                this.txtTagsExclude.Text = Settings.Default.lastSpecflowExclude;
                this.chkAutoOpen.Checked = Settings.Default.openReportAfterRun;
                this.chkArtifactOverride.Checked = Settings.Default.overrideArtifactSubDir;
                this.txtArtifactSubDir.Text = Settings.Default.artifactSubDir;
                this.chkSpecFlowReport.Checked = Settings.Default.createSpecFlowReport;
                this.chkNunitReport.Checked = Settings.Default.createNUnitReport;
                this.chkReportUnit.Checked = Settings.Default.createReportUnit;
                this.chkAppiumURI.Checked = Settings.Default.overrideAppiumURI;
                this.txtAppiumURI.Text = Settings.Default.appiumURI;
            }
        }

        private void LoadMonoSettings()
        {
            String configFileName = "TestRunnerWin.exe.config";
            string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); 
            string configFile = Path.Combine(appPath, configFileName);
            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = configFile;
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

            this.txtConfig.Text = ConfigurationManager.AppSettings["lastConfig"];
            this.txtDLL.Text = ConfigurationManager.AppSettings["lastDll"];
            this.txtProject.Text = ConfigurationManager.AppSettings["lastProject"];
            this.txtTagsInclude.Text = ConfigurationManager.AppSettings["lastSpecflowInclude"];
            this.txtTagsExclude.Text = ConfigurationManager.AppSettings["lastSpecflowExclude"];
            this.chkAutoOpen.Checked = Convert.ToBoolean(ConfigurationManager.AppSettings["openReportAfterRun"]);
            this.chkArtifactOverride.Checked = Convert.ToBoolean(ConfigurationManager.AppSettings["overrideArtifactSubDir"]);
            this.txtArtifactSubDir.Text = ConfigurationManager.AppSettings["artifactSubDir"];
            this.chkAppiumURI.Checked = Convert.ToBoolean(ConfigurationManager.AppSettings["overrideAppiumURI"]);
            this.txtAppiumURI.Text = ConfigurationManager.AppSettings["appiumURI"];
            this.chkSpecFlowReport.Checked = Convert.ToBoolean(ConfigurationManager.AppSettings["createSpecFlowReport"]);
            this.chkNunitReport.Checked = Convert.ToBoolean(ConfigurationManager.AppSettings["createNUnitReport"]);
            this.chkReportUnit.Checked = Convert.ToBoolean(ConfigurationManager.AppSettings["createReportUnit"]);
        }

        private void SaveSettings()
        {
            if (isMono)
            {
                this.SaveMonoSettings();
            }
            else
            {
                Settings.Default.lastProject = this.txtProject.Text;
                Settings.Default.lastConfig = this.txtConfig.Text;
                Settings.Default.lastDLL = this.txtDLL.Text;
                Settings.Default.lastSpecflowInclude = this.txtTagsInclude.Text;
                Settings.Default.lastSpecflowExclude = this.txtTagsExclude.Text;
                Settings.Default.openReportAfterRun = this.chkAutoOpen.Checked;
                Settings.Default.artifactSubDir = this.txtArtifactSubDir.Text;
                Settings.Default.overrideArtifactSubDir = this.chkArtifactOverride.Checked;
                Settings.Default.createSpecFlowReport = this.chkSpecFlowReport.Checked;
                Settings.Default.createNUnitReport = this.chkNunitReport.Checked;
                Settings.Default.createReportUnit = this.chkReportUnit.Checked;
                Settings.Default.appiumURI = this.txtAppiumURI.Text;
                Settings.Default.overrideAppiumURI = this.chkAppiumURI.Checked;
                Settings.Default.Save();
            }
        }

        private void SaveMonoSettings()
        {
            String configFileName = "TestRunnerWin.exe.config";
            string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string configFile = Path.Combine(appPath, configFileName);
            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = configFile;
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

			if (config.AppSettings.Settings ["lastConfig"] == null) {
				config.AppSettings.Settings.Add ("lastConfig", this.txtConfig.Text);
			} else {
				config.AppSettings.Settings["lastConfig"].Value = this.txtConfig.Text;
			}

			if (config.AppSettings.Settings ["lastDll"] == null) {
				config.AppSettings.Settings.Add ("lastDll", this.txtDLL.Text);
			} else {
				config.AppSettings.Settings["lastDll"].Value = this.txtDLL.Text;
			}

			if (config.AppSettings.Settings ["lastProject"] == null) {
				config.AppSettings.Settings.Add ("lastProject", this.txtProject.Text);
			} else {
				config.AppSettings.Settings["lastProject"].Value = this.txtProject.Text;
			}

			if (config.AppSettings.Settings ["lastSpecflowInclude"] == null) {
				config.AppSettings.Settings.Add ("lastSpecflowInclude", this.txtTagsInclude.Text);
			} else {
				config.AppSettings.Settings["lastSpecflowInclude"].Value = this.txtTagsInclude.Text;
			}

			if (config.AppSettings.Settings ["lastSpecflowExclude"] == null) {
				config.AppSettings.Settings.Add ("lastSpecflowExclude", this.txtTagsExclude.Text);
			} else {
				config.AppSettings.Settings["lastSpecflowExclude"].Value = this.txtTagsExclude.Text;
			}

			if (config.AppSettings.Settings ["openReportAfterRun"] == null) {
				config.AppSettings.Settings.Add ("openReportAfterRun", this.chkAutoOpen.Checked.ToString());
			} else {
				config.AppSettings.Settings["openReportAfterRun"].Value = this.chkAutoOpen.Checked.ToString();
			}

            if (config.AppSettings.Settings["overrideArtifactSubDir"] == null)
            {
                config.AppSettings.Settings.Add("overrideArtifactSubDir", this.chkArtifactOverride.Checked.ToString());
            }
            else
            {
                config.AppSettings.Settings["overrideArtifactSubDir"].Value = this.chkArtifactOverride.Checked.ToString();
            }

            if (config.AppSettings.Settings["artifactSubDir"] == null)
            {
                config.AppSettings.Settings.Add("artifactSubDir", this.chkArtifactOverride.Checked.ToString());
            }
            else
            {
                config.AppSettings.Settings["artifactSubDir"].Value = this.chkArtifactOverride.Checked.ToString();
            }

            if (config.AppSettings.Settings["overrideAppiumURI"] == null)
            {
                config.AppSettings.Settings.Add("overrideAppiumURI", this.chkAppiumURI.Checked.ToString());
            }
            else
            {
                config.AppSettings.Settings["overrideAppiumURI"].Value = this.chkAppiumURI.Checked.ToString();
            }

            if (config.AppSettings.Settings["appiumURI"] == null)
            {
                config.AppSettings.Settings.Add("appiumURI", this.txtAppiumURI.Text);
            }
            else
            {
                config.AppSettings.Settings["appiumURI"].Value = this.txtAppiumURI.Text;
            }

            if (config.AppSettings.Settings["createSpecFlowReport"] == null)
            {
                config.AppSettings.Settings.Add("createSpecFlowReport", this.chkNunitReport.Checked.ToString());
            }
            else
            {
                config.AppSettings.Settings["createSpecFlowReport"].Value = this.chkNunitReport.Checked.ToString();
            }

            if (config.AppSettings.Settings["createNUnitReport"] == null)
            {
                config.AppSettings.Settings.Add("createNUnitReport", this.chkNunitReport.Checked.ToString());
            }
            else
            {
                config.AppSettings.Settings["createNUnitReport"].Value = this.chkNunitReport.Checked.ToString();
            }


            if (config.AppSettings.Settings["createReportUnit"] == null)
            {
                config.AppSettings.Settings.Add("createReportUnit", this.chkReportUnit.Checked.ToString());
            }
            else
            {
                config.AppSettings.Settings["createReportUnit"].Value = this.chkReportUnit.Checked.ToString();
            }

            config.Save();
        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            this.rtbOutput.Clear();
        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            this.SaveSettings();
        }

        private void chkArtifactOverride_CheckedChanged(object sender, EventArgs e)
        {
            this.txtArtifactSubDir.Enabled = this.chkArtifactOverride.Checked;
        }

        private void chkSpecFlowReport_CheckedChanged(object sender, EventArgs e)
        {
            this.txtProject.Enabled = this.btnChooseProject.Enabled = this.chkSpecFlowReport.Checked;
        }

        private void llbTagHelp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://nunit.org/index.php?p=consoleCommandLine&r=2.6.4");
        }

        private void chkAppiumURI_CheckedChanged(object sender, EventArgs e)
        {
            this.txtAppiumURI.Enabled = this.chkAppiumURI.Checked;
        }
    }
}
