﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

//Author : Nancy Malgani

namespace UnitTestProject4
{
    public class HomePage
    {
        IWebDriver driver;
        private readonly By ButtonContactUs = By.XPath("//div[@id='container']//li[@class='Contact_Us']/a");
        private readonly By TextHours = By.XPath("//tr[4]//td[@class='hours - text']");
        private readonly By TextAddress = By.XPath("//p[2]/span[@class='h4'][1]");
        private readonly By TextPhone = By.XPath("//p[2]/span[@class='h4']/a");
        private readonly By TextEmail = By.XPath("//div[@class='col-xs-12 col-sm-4'][3]/p[2]/span[@class='h4']/a");

        public void LaunchWebsite()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.bcferries.com");
        }

        public void NavigateToContactUs()
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
            driver.FindElement(ButtonContactUs).Click();
        }

        public void ScrollToBottomOfPage()
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
        }

        public void VerifyTextHours(string p0)
        {
            IWebElement body = driver.FindElement(By.TagName("body"));
            Assert.IsTrue(body.Text.Contains(p0));
        }

        public void VerifyTextAddress(string p0)
        {
            IWebElement body = driver.FindElement(By.TagName("body"));
            Assert.IsTrue(body.Text.Contains(p0));
        }

        public void VerifyTextPhone(string p0)
        {
            IWebElement body = driver.FindElement(By.TagName("body"));
            Assert.IsTrue(body.Text.Contains(p0));
        }

        public void VerifyTextEmail(string p0)
        {
            IWebElement body = driver.FindElement(By.TagName("body"));
            Assert.IsTrue(body.Text.Contains(p0));
        }

        public void TearDownAfterScenario()
        {
            driver.Quit();
        }
    }
}

