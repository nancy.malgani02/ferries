﻿Feature: WebsiteDemo
	In order to ensure clients can contact us
	As a tester
	I want to verify presence of mandatory fields on contact us page

@WebsiteTesting
Scenario: Verify Contact us page
	Given I launch the website
	When I navigate to contact us
	And I scroll to BC Ferries Vacations on bottom of page
	Then I should see Hours as '9am - 6pm Monday to Friday'
	And I should see Address as '1010 Canada Place'
	And I should see Phone as '1-888-BC FERRY'
	And I should see Email as 'bcferriesvacations.com'
