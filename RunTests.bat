
:: Execute Tests
echo "Execute Tests"
call packages\NUnit.Runners.2.6.4\tools\nunit-console.exe -timeout=2000000 -include=RestApiDemo,WebsiteTesting -xml=./UnitTestProject4/bin/Debug/TestResults/TestResults.xml ./UnitTestProject4/bin/Debug/UnitTestProject4.dll | tee ./UnitTestProject4/bin/Debug/TestResults/TestResults.txt

:: Generate Reports
echo "Generate Reports"
call packages\SpecFlow.2.2.0\tools\specflow.exe nunitexecutionreport ./UnitTestProject4/SampleApiDemo.csproj /out:./UnitTestProject4/bin/Debug/TestResults/TestExecutionReport.html /xmlTestResult:./UnitTestProject4/bin/Debug/TestResults/TestResults.xml /testOutput:./UnitTestProject4/bin/Debug/TestResults/TestResults.txt /xsltFile:./TestRunnerWin/NUnitExecutionReport.xslt